LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
use ieee.std_logic_arith.all;

ENTITY dff_re IS

	PORT (D, clk, clr : IN std_logic;
		    nQ, Q : OUT std_logic
	);
END ENTITY;

ARCHITECTURE applyD OF dff_re IS

BEGIN

	PROCESS(clk, clr)
	BEGIN
		if (clr = '1') then
			Q <= '0';
		elsif rising_edge(clk) THEN
			Q <= D;
			nQ <= not D;
		END IF;
	END PROCESS;
	
END applyD;
