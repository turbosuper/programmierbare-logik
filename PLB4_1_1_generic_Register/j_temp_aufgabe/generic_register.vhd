library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

ENTITY generic_register IS
  GENERIC (m : INTEGER := 8);
  PORT (
      clk       : in  std_logic;
      clr      : in  std_logic;
      generic_LD   : in  std_logic_vector (0 to m-1); 
      generic_D   : in  std_logic_vector (0 to m-1); 
      generic_reg_Q      : out std_logic_vector(0 to m-1);
      generic_reg_nQ      : out std_logic_vector(0 to m-1)         
      );
END generic_register;

ARCHITECTURE arch OF generic_register IS

COMPONENT one_bit_register IS
	PORT (
      clk, clr, LD, D : in  std_logic;
      reg_Q, reg_nQ   : out std_logic
	);
END COMPONENT;

BEGIN
	reg_gen: FOR i in 0 TO m-1 GENERATE
		onebitreg: one_bit_register 
		PORT MAP( clk => clk, clr => clr, LD => generic_LD(i), D => generic_D(i), reg_Q => generic_reg_Q(i), reg_nQ => generic_reg_nQ(i)); -- nQ => reg_nQ);
	END GENERATE;
 	
END arch;
  
