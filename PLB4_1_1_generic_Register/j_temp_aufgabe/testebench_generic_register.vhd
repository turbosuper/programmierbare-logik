library ieee;                                                                                                                            
use ieee.std_logic_1164.all;                                                                                                             
ENTITY testbench_generic_register is                                                                                                         
                                                                                                                                         
END testbench_generic_register;                                                                                                              
                                                                                                                                         
                                                                                                                                         
ARCHITECTURE test OF testbench_generic_register IS                                                                                           
                                                                                                                                         
COMPONENT generic_register IS
  GENERIC (m : INTEGER := 8);
  PORT (
      clk, clr : in  std_logic;
      generic_LD, generic_D   : in  std_logic_vector (0 to m-1); 
      generic_reg_Q , generic_reg_nQ     : out std_logic_vector(0 to m-1)
      );

END COMPONENT ;
	constant m : INTEGER := 8;                                                                                                                                         
	constant small_period: time := 10 ns;
	constant middle_period: time :=80 ns;
	constant big_period: time := 160 ns;
	constant longest_period: time := 840 ns;
	SIGNAL tbclk : std_logic := '0';                                                                                                           
	SIGNAL tbclr : std_logic := '0';                                                                                                           
	SIGNAL tbD : std_logic_vector (0 to m-1) := "00000000";                                                                                                           
	SIGNAL tbLD : std_logic_vector (0 to m-1) := "00000000";                                                                                                           
	SIGNAL tbreg_Q : std_logic_vector (0 to m-1) := "00000000";                                                                                                           
	SIGNAL tbreg_nQ : std_logic_vector (0 to m-1) := "00000000";                                                                                                           
                                                                                                                                         
	BEGIN                                                                                                                                    
                                                                                                                                         
	dut: generic_register                                                                                                                        
                                                                                                                                         
	PORT MAP (                                                                                                                               
		clk => tbclk,                                                                                                                            
		clr => tbclr,                                                                                                                              
		generic_D => tbD,                                                                                                                              
		generic_LD => tbLD,                                                                                                                              
		generic_reg_Q => tbreg_Q,                                                                                                                            
		generic_reg_nQ => tbreg_nQ                                                                                                                               
      );                                                                                                                                 
                                                                                                                                         
tbclr <= not tbclr AFTER longest_period;
tbclk <=  not tbclk AFTER small_period;
tbD <=  "00000111" AFTER 30 ns, "11100100" AFTER 80 ns, "00111100" AFTER 120 ns, "10101010" AFTER 160 ns, "01100110" AFTER 200 ns, "00000000" AFTER 250 ns, "11111111" AFTER 350 ns;
tbLD <= "11111111" AFTER 20 ns, "00000000" AFTER 70 ns, "10011100" AFTER 90 ns, "11111111" AFTER 105 ns, "10100000" AFTER 150 ns, "11111111" AFTER 210 ns, "01010101" AFTER 400 ns;
end test;

