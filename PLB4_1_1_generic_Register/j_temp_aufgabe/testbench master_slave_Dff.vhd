library ieee;                                                                                                                            
use ieee.std_logic_1164.all;                                                                                                             
ENTITY testbench_master_slave_Dff is                                                                                                         
                                                                                                                                         
END testbench_master_slave_Dff;                                                                                                              
                                                                                                                                         
                                                                                                                                         
ARCHITECTURE test OF testbench_master_slave_Dff IS                                                                                           
                                                                                                                                         
	COMPONENT master_slave_Dff IS                                                                                                                
        PORT (                                                                                                                           
                clk, Din, reset : in std_logic;                                                                                               
                Q, nQ : out std_logic                                                                                                      
                    );                                                                                                                   
                                                                                                                                         
	END COMPONENT;                                                                                                                           
                                                                                                                                         
	constant small_period: time := 20 ns;
	constant middle_period: time :=80 ns;
	constant big_period: time := 160 ns;
	constant longest_period: time := 640 ns;
	SIGNAL tbclk : std_logic := '0';                                                                                                           
	SIGNAL tbDin : std_logic := '0';                                                                                                           
	SIGNAL tbreset : std_logic := '0';                                                                                                           
	SIGNAL tbQ : std_logic := '0';                                                                                                           
	SIGNAL tbnQ : std_logic := '0';                                                                                                           
                                                                                                                                         
	BEGIN                                                                                                                                    
                                                                                                                                         
	dut: master_slave_Dff                                                                                                                        
                                                                                                                                         
	PORT MAP (                                                                                                                               
		clk => tbclk,                                                                                                                            
		Din => tbDin,                                                                                                                              
		reset => tbreset,                                                                                                                              
		Q => tbQ,                                                                                                                            
		nQ => tbnQ                                                                                                                               
      );                                                                                                                                 
                                                                                                                                         
tbreset <= not tbreset AFTER longest_period;
tbDin <=  not tbDin AFTER big_period;
tbclk <=  not tbclk AFTER small_period;

end test;

-- vsim work.testbench_master_slave_dff; add wave -position insertpoint sim:/testbench_master_slave_dff/dut/* ; add wave -position insertpoint sim:/testbench_master_slave_dff/dut/first_ff/*; add wave -position insertpoint sim:/testbench_master_slave_dff/dut/second_ff/*; run 1000ns
