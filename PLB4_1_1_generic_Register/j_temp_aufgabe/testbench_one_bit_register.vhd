library ieee;                                                                                                                            
use ieee.std_logic_1164.all;                                                                                                             
ENTITY testbench_one_bit_register is                                                                                                         
                                                                                                                                         
END testbench_one_bit_register;                                                                                                              
                                                                                                                                         
                                                                                                                                         
ARCHITECTURE test OF testbench_one_bit_register IS                                                                                           
                                                                                                                                         
COMPONENT one_bit_register IS
  PORT (
      clk, clr, LD, D : in  std_logic;
      reg_Q, reg_nQ      : out std_logic         
      );

END COMPONENT ;
                                                                                                                                         
	constant small_period: time := 10 ns;
	constant middle_period: time :=80 ns;
	constant big_period: time := 160 ns;
	constant longest_period: time := 840 ns;
	SIGNAL tbclk : std_logic := '0';                                                                                                           
	SIGNAL tbD : std_logic := '0';                                                                                                           
	SIGNAL tbLD : std_logic := '0';                                                                                                           
	SIGNAL tbclr : std_logic := '0';                                                                                                           
	SIGNAL tbreg_Q : std_logic := '0';                                                                                                           
	SIGNAL tbreg_nQ : std_logic := '0';                                                                                                           
                                                                                                                                         
	BEGIN                                                                                                                                    
                                                                                                                                         
	dut: one_bit_register                                                                                                                        
                                                                                                                                         
	PORT MAP (                                                                                                                               
		clk => tbclk,                                                                                                                            
		D => tbD,                                                                                                                              
		LD => tbLD,                                                                                                                              
		clr => tbclr,                                                                                                                              
		reg_Q => tbreg_Q,                                                                                                                            
		reg_nQ => tbreg_nQ                                                                                                                               
      );                                                                                                                                 
                                                                                                                                         
tbclr <= not tbclr AFTER longest_period;
tbD <=  not tbD AFTER middle_period;
tbLD <=  not tbLD AFTER big_period;
tbclk <=  not tbclk AFTER small_period;

end test;

