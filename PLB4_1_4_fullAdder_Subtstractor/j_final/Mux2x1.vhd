library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

ENTITY Mux2x1 IS

	PORT(
		IA, IB, S0 : IN std_logic;
		Y : OUT std_logic
	);

END ENTITY;

ARCHITECTURE behaviour OF Mux2x1 IS
BEGIN

	PROCESS(S0, IA, IB)
	BEGIN
		IF (S0 = '0') THEN
			y <= IA;
		else
			y <= IB;
		END IF;
	END PROCESS;


END behaviour;