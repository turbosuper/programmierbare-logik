library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

Entity crAS IS
	GENERIC (m : Integer := 4);
	Port(Xin, Yin : in std_logic_vector(m-1 downto 0);
			cbi, AS, clr, clk : in std_logic;
			sd : out std_logic_vector(m-1 downto 0);
			cbo : out std_logic
	);

End Entity;

ARCHITECTURE GenCrAS of crAS IS

	COMPONENT AddSubtr IS

		Port(x, y, cbi, AS : in std_logic;
				sd, cbo : out std_logic
		);

	End COMPONENT;
	
	COMPONENT falling_edge_Dff IS
		PORT(
		d : in std_logic;
		clear : in std_logic;
		clk : in std_logic;
		q : out std_logic 
		);
	
	END COMPONENT ;

	COMPONENT dff_re IS
	
		PORT (D, clk, clr : IN std_logic;
				Q, NQ : OUT std_logic
		);
	END COMPONENT;
	
	SIGNAL cbBus : std_logic_vector(m downto 0); -- this bus with m width (internal carry signals + 1 cin + 1 cout)
	SIGNAL xBUS, yBUS, sdBUS : std_logic_vector(m-1 downto 0);
	
Begin

	XinLines : FOR i IN m-1 DOWNTO 0 GENERATE
		XinFF : dff_re
		PORT MAP(
					D => Xin(i),
					q => xBUS(i),
					clk => clk,
					clr => clr
		);
	END GENERATE;
	
	YinLines : FOR i IN m-1 DOWNTO 0 GENERATE
		YinFF : dff_re
			PORT MAP(
						D => Yin(i),
						q => yBUS(i),
						clk => clk,
						clr => clr
			);
	END GENERATE;
	
	ASLines : FOR i IN m-1 DOWNTO 0 GENERATE
		A_S : AddSubtr
			PORT MAP(
						x => xBUS(i),
						y => yBUS(i),
						cbi => cbBus(i),
						AS => AS,
						sd => sdBUS(i),
						cbo => cbBus(i+1)
			);	
	END GENERATE;
	
	sdLines : FOR i IN m-1 DOWNTO 0 GENERATE
		sdoFF : falling_edge_Dff
		PORT MAP(
					d => sdBUS(i),
					q => sd(i),
					clk => clk,
					clear => clr
		);
	END GENERATE;
	
	cbBus(0) <= cbi;
	cbo <= cbBus(m);

END GenCrAS;