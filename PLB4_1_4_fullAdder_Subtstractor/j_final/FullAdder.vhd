library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

Entity FullAdder IS

	Port( x,y, cin : IN std_logic;
			s, cout : OUT std_logic
	);

End Entity;

ARCHITECTURE TwoBitCarryAdder of FullAdder IS

	COMPONENT HalfAdder is

		Port(a, b : IN std_logic;
				s, c : OUT std_logic
		);

	End COMPONENT;

	SIGNAL intSum, intCarry1, intCarry2 : std_logic;
Begin

	HA1 : HalfAdder
	PORT MAP (a => x,
				b => y,
				s => intSum,
				c => intCarry1
	);
	
	HA2 : HalfAdder
	PORT MAP( a => intSum,
				b => cin,
				s => s,
				c => intCarry2
	);
	
	cout <= intCarry2 or intCarry1;

End TwoBitCarryAdder;