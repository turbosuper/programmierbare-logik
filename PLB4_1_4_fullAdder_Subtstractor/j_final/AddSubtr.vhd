library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

Entity AddSubtr IS
	GENERIC (m : INTEGER := 4);
	Port(a, b : in std_logic_vector (m-1 downto 0);
	 AS : in std_logic;
		s_out :out std_logic_vector (m-1 downto 0)
	);

End Entity;

ARCHITECTURE Add_or_Subtract of AddSubtr IS

	COMPONENT FullAdder is
		Port( x,y, cin : IN std_logic;
				s, cout : OUT std_logic
		);
	End COMPONENT;
	
	SIGNAL intern_cout, intern_b : std_logic_vector (m-1 downto 0);
	

BEGIN

	Adders_chain : FOR i IN 1 TO m-1 GENERATE
		Last_Full_Adders: FullAdder
		PORT MAP( x => a(i),
				 y => intern_b(i),
				 cin => intern_cout(i-1),
				 cout => intern_cout(i),
				 s => s_out(i) 
		);
	END GENERATE;

	First_Full_Adder: FullAdder
	PORT MAP( x => a(0),
				 y => intern_b(0),
				 cin => AS,
				 cout => intern_cout(0),
				 s => s_out(0) 
	);

	intern_b <= b XOR (AS & AS & AS & AS);
	

END Add_or_Subtract;