library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

Entity HalfAdder is

	Port(a, b : IN std_logic;
			s, c : OUT std_logic
	);

End Entity;

ARCHITECTURE TwoBitAdd of HalfAdder IS


Begin

c <= a and b;
s <= a xor b;

End TwoBitAdd;