library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

Entity AddSubtr IS

	Port(x, y, cbi, AS : in std_logic;
			sd, cbo : out std_logic
	);

End Entity;

ARCHITECTURE Add_or_Subtract of AddSubtr IS

	COMPONENT FullAdder is
		Port( x,y, cin : IN std_logic;
				s, cout : OUT std_logic
		);
	End COMPONENT;
	
	COMPONENT Mux2x1 IS

	PORT(
		IA, IB, S0 : IN std_logic;
		Y : OUT std_logic
	);

	END COMPONENT;
	
	SIGNAL intY, notY : std_logic;

Begin

	Mux : Mux2x1
	PORT MAP( IA => y,
				 IB => notY,
				 S0 => AS,
				 Y => intY
	);
	
	FA : FullAdder
	PORT MAP( x => x,
				 y => intY,
				cin => cbi,
				s => sd,
				cout => cbo
	);
	
	notY <= not y;

END Add_or_Subtract;