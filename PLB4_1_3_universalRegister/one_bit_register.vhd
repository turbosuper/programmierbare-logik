library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


ENTITY one_bit_register IS
  PORT
    (
      clk       : in  std_logic;
      clr      : in  std_logic;
      LD   : in  std_logic; 
      D   : in  std_logic := '0'; 
      --
      reg_Q      : out std_logic;
      reg_nQ      : out std_logic         
      );
end one_bit_register;

ARCHITECTURE arch OF one_bit_register IS

COMPONENT master_slave_Dff IS
	PORT (
	clk ,Din , reset: in std_logic;
	Q, nQ : out std_logic
	);
END COMPONENT;

SIGNAL  s1, s2, s3, q_int  : std_logic;

BEGIN
MS_DFF: master_slave_Dff 
	PORT MAP( clk => clk, reset => clr, Din => s3, Q => q_int); -- nQ => reg_nQ);


  s1 <= not LD AND q_int;
  s2 <= D AND LD;
  s3 <= s1 OR s2;
  
  sync_proc : process (clk, clr) is
  begin
	if (clr = '1') THEN --asynchronus reset
		reg_Q <= '0';
		reg_nQ <= '1';
	ELSIF rising_edge(clk) THEN
		reg_Q <= q_int; 
		reg_nQ <= not q_int;
      end if;
  end process;
END arch;
  
