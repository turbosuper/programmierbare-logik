library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

ENTITY universal_register IS
	GENERIC (number_of_bits : INTEGER := 4);
	PORT (
	S0, S1 : in std_logic; --mode control input
	SHR, SHL : in std_logic; --shift right, left
	P : in std_logic_vector(number_of_bits-1 downto 0);
	clk: in std_logic;
	reset : in std_logic;
	Q : out std_logic_vector(number_of_bits-1 downto 0)
	);
END ENTITY;

ARCHITECTURE behaviour OF universal_register IS

SIGNAL Qintern: std_logic_vector (number_of_bits-1 downto 0);

BEGIN
main_logic: PROCESS(clk, reset)
	BEGIN 
		IF (reset = '1') THEN 
			Qintern <= ( others => '0');
		ELSIF rising_edge(clk) THEN
			IF (S0 = '1') AND (S1 ='1') THEN --paralel load
				Qintern <= P;
			ELSIF (S0 = '0') AND (S1 ='0') THEN --hold
				Qintern <= Qintern;
			ELSIF (S0 = '1') AND (S1 ='0') THEN --shift left
				Qintern <= Qintern(number_of_bits-2 downto 0) & SHL;
			ELSIF (S0 = '0') AND (S1 ='1') THEN --shift right
				Qintern <= SHR & Qintern(number_of_bits-1 downto 1); 
			END IF;
		END IF;

	END PROCESS main_logic;

	Q <= Qintern;

END behaviour;
