library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

ENTITY master_slave_Dff IS
	PORT (
	clk : in std_logic;
	Din : in std_logic;
	reset : in std_logic;
	Q : out std_logic;
	nQ : out std_logic
	);
END ENTITY;

ARCHITECTURE behaviour OF master_slave_Dff IS

	COMPONENT dff_re IS
		PORT (D, clk, clr : IN std_logic;
				 nQ, Q : OUT std_logic
		);
	END COMPONENT;
	
	COMPONENT falling_edge_Dff IS
		PORT(
		d, clear, clk : in std_logic;
		q : out std_logic 
		);
	END COMPONENT ;

	SIGNAL data : std_logic;
	
BEGIN
	
	first_ff: dff_re 
	PORT MAP (
	D =>Din, clk=>clk, clr=>reset, Q=> data);

	second_ff: falling_edge_Dff 
	PORT MAP (
	d => data, clk=>clk, clear=>reset, q => Q);

END behaviour;
		
