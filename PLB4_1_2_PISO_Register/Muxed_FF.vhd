library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

ENTITY Muxed_FF IS

	PORT(
		D0, D1, S0, clk, clr : IN std_logic;
		Q : OUT std_logic
	);

END ENTITY;

ARCHITECTURE behaviour OF Muxed_FF IS

	SIGNAL Dx: std_logic;
	
	COMPONENT Mux2x1 IS
	PORT(
		IA, IB, S0 : IN std_logic;
		Y : OUT std_logic
	);
	END COMPONENT;
	
	COMPONENT master_slave_Dff IS
	PORT (
	clk : in std_logic;
	Din : in std_logic;
	reset : in std_logic;
	Q : out std_logic;
	nQ : out std_logic
	);
	END COMPONENT;

BEGIN

	FirstMux : Mux2x1
	PORT MAP(
				IA => D0,
				IB => D1,
				S0 => S0,
				Y => Dx
	);
	
	FirstFF : master_slave_Dff
	PORT MAP(
				Din => Dx,
				clk => clk,
				reset => clr,
				Q => Q
	);

END behaviour;