library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

ENTITY piso_register IS
	GENERIC (m : INTEGER := 4);
	PORT (
	clk : in std_logic;
	D : in std_logic_vector(0 to m-1);
	reset : in std_logic;
	q : out std_logic
	);
END ENTITY;

ARCHITECTURE behaviour OF piso_register IS
	
	COMPONENT dt_clkdiv IS
	GENERIC(k : INTEGER := 4);
	PORT (reset : IN std_logic;
			clk_in : IN std_logic;
			clk_out : OUT std_logic
	);
	END COMPONENT;
	
	COMPONENT Muxed_FF IS

	PORT(
		D0, D1, S0, clk, clr : IN std_logic;
		Q : OUT std_logic
	);

	END COMPONENT;
	
	SIGNAL ConstZero : std_logic := '0';
	SIGNAL divclk : std_logic;
	SIGNAL Transf : std_logic_vector(0 to m-2);
	
BEGIN

	clkdiv : dt_clkdiv
	GENERIC MAP (k => m)
	PORT MAP(clk_in => clk,
				clk_out => divclk,
				reset => reset
	);
	
	First : Muxed_FF
	PORT MAP(
				D0 => ConstZero,
				D1 => D(0),
				S0 => divclk,
				clk => clk,
				clr => reset,
				Q => Transf(0)
	);
	
	OtherFF : FOR i IN 1 TO m-2 GENERATE
		MiddleFF : Muxed_FF
		PORT MAP(
					D0 => Transf(i-1),
					D1 => D(i),
					S0 => divclk,
					clk => clk,
					clr => reset,
					Q => Transf(i)
		);
	
	END GENERATE;
	
	Last : Muxed_FF
	PORT MAP(
				D0 => Transf(m-2),
				D1 => D(m-1),
				S0 => divclk,
				clk => clk,
				clr => reset,
				Q => q
	);

END behaviour;