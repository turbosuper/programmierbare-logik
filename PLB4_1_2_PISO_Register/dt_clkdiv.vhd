LIBRARY ieee ;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY dt_clkdiv IS
	GENERIC(k : INTEGER := 4);
	PORT (reset : IN std_logic;
			clk_in : IN std_logic;
			clk_out : OUT std_logic
	);
END dt_clkdiv;

ARCHITECTURE behavior OF dt_clkdiv IS

SIGNAL s_count : integer := 0;

BEGIN

	counterGen : PROCESS (reset, clk_in)
	BEGIN
		IF (reset = '1') THEN
			s_count <= k;
		ELSIF (clk_in'event AND clk_in = '1') THEN
			IF (s_count = k) THEN 
				s_count <= 0;
			else
				s_count <= s_count + 1;
			end if;
		end if;
		
	END PROCESS counterGen;
	
	clkgen : PROCESS (reset, s_count)
	BEGIN
		IF (reset = '1') THEN
			clk_out <= '1';
		ELSIF(s_count < k) THEN
					clk_out <= '0';
			ELSE
					clk_out <= '1';
			END IF;
	END PROCESS clkgen;
	
END behavior;