library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

ENTITY falling_edge_Dff IS
	PORT(
	d : in std_logic;
	clear : in std_logic;
	clk : in std_logic;
	q : out std_logic 
);

END ENTITY;
	
ARCHITECTURE behaviour OF falling_edge_Dff IS


BEGIN

	PROCESS(clk,clear)
	BEGIN
		IF (clear = '1') THEN
			q <= '0';
		ELSIF falling_edge(clk) THEN
			q <= d;
		END IF;	
	END PROCESS;
	
END behaviour;
