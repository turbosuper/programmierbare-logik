library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

ENTITY register_muio_generic IS
	GENERIC (m : INTEGER := 4);
	PORT (
	clk : in std_logic;
	LD : in std_logic;
	ser_in, ser_out: in std_logic;
	D : in std_logic_vector(0 to m-1);
	reset : in std_logic;
	q : out std_logic_vector(0 to m-1)
	);
END ENTITY;

ARCHITECTURE behaviour OF register_generic IS

	COMPONENT falling_edge_Dff IS
		PORT(
		d : in std_logic;
		clear : in std_logic;
		clk : in std_logic;
		q : out std_logic 
		);
	
	END COMPONENT ;

	COMPONENT dff_re IS
	
		PORT (D, clk, clr : IN std_logic;
				Q, NQ : OUT std_logic
		);
	END COMPONENT;

	SIGNAL nLD : std_logic;
	SIGNAL Q1, Q2, Da, Db, Di : std_logic_vector(0 to m-1);
	
BEGIN

	BitLines : FOR i IN 0 TO m-1 GENERATE
		masterFF : dff_re
		PORT MAP(
					D => Di(i),
					q => Q1(i),
					clk => clk,
					clr => reset
		);
	
		slaveFF : falling_edge_Dff
		PORT MAP(
					d => Q1(i),
					q => Q2(i),
					clk => clk,
					clear => reset
		);
	END GENERATE;
	
	PROCESS (D, LD)
	BEGIN
		FOR i IN 0 TO m-1 LOOP
			Db(i) <= LD and D(i);
		END LOOP;
	END PROCESS;
	
	PROCESS (Q2, nLD)
	BEGIN
		FOR i IN 0 TO m-1 LOOP
			Da(i) <= nLD and Q2(i);
		END LOOP;
	END PROCESS;
	
	PROCESS (Da, Db)
	BEGIN
		FOR i IN 0 TO m-1 LOOP
			Di(i) <= Da(i) or Db(i);
		END LOOP;
	END PROCESS;
	
	q <= Q2;
	nLD <= not LD;
	
END behaviour;
