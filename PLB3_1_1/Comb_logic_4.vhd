library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
 
ENTITY Comb_logic_4 IS
    PORT (
    x : in std_logic;
    y : in std_logic;
    z : in std_logic;
    v : in std_logic;
    f : out std_logic
    );
END Comb_logic_4;

ARCHITECTURE behave OF Comb_logic_4 IS    
BEGIN
f <= (NOT x OR  NOT z) AND (y OR NOT v OR z) AND (v OR x OR NOT z);
END behave;