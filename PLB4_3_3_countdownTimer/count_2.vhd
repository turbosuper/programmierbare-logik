LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

Entity count_2 IS
	PORT(clk, clr : IN std_logic;
			number : OUT std_logic_vector(3 downto 0);
			tick : OUT std_logic
	);

END ENTITY;

Architecture mwcnt OF count_2 IS

	Type Zustaende IS (Z0, Z1);
	Signal Z, FZ : Zustaende;
Begin

	Zustandsspeicher: PROCESS(clk, clr)
	Begin
		if(clr='1') then
			Z <= Z0;
		elsif (clk'event AND clk='0') then
			Z <= FZ;
		end if;
	End PROCESS Zustandsspeicher;

	Transition : PROCESS(clr, Z)
	Begin
		case Z IS
		when Z0 => FZ <= Z1;
		when Z1 => FZ <= Z0;
		end case;
	End PROCESS Transition;  
	
	Ausgang : PROCESS(Z)
		Begin
		tick <= '0';
		case Z IS
		when Z0 => number <= "0000";
		when Z1 => number <= "0001";
			tick <= '1';
		end case;
	END PROCESS Ausgang;
	
End mwcnt;