library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

ENTITY seven_seg_disp IS
	PORT (
	LTN : in std_logic; -- general enable
	BLN : in std_logic; --Test for all segments
	HB : in std_logic;
	D : in std_logic_vector(3 downto 0);
	segm : out std_logic_vector(0 to 6) --Display segments
	);
END ENTITY;

ARCHITECTURE behaviour OF seven_seg_disp IS
	BEGIN
	
	PROCESS(D, LTN, BLN) IS
	BEGIN
		IF (LTN = '0') AND (BLN = '0') THEN
			segm <= "1111111"; -- off
		ELSIF (LTN = '0') AND (BLN = '1') THEN
			segm <= "0000000"; -- test, all on
		ELSIF (LTN = '1') AND (BLN = '0') THEN
			CASE D IS
				WHEN "0000"=> segm  <="0000001"; --0
				WHEN "0001"=> segm  <="1001111"; --1
				WHEN "0010"=> segm  <="0010010"; --2 
				WHEN "0011"=> segm  <="0000110"; --3 
				WHEN "0100"=> segm  <="1001100"; --4 
				WHEN "0101"=> segm  <="0100100"; --5 
				WHEN "0110"=> segm  <="0100000"; --6 
				WHEN "0111"=> segm  <="0001101"; --7 
				WHEN "1000"=> segm  <="0000000"; --8 
				WHEN "1001"=> segm  <="0000100"; --9 
				WHEN "1010"=> segm  <="0001000"; --A 
				WHEN "1011"=> segm  <="1100000"; --b 
				WHEN "1100"=> segm  <="0110001"; --C 
				WHEN "1101"=> segm  <="1000010"; --d 
				WHEN "1110"=> segm  <="0110000"; --E 
				WHEN "1111"=> segm  <="0111000"; --F 	
				WHEN OTHERS => segm <="0000000"; 
			END CASE;
			IF HB = '1' THEN
				IF D > "1001" THEN
					segm <= "1111111";
				END IF;
			END IF;
			
		END IF;
	END PROCESS;
END behaviour;
