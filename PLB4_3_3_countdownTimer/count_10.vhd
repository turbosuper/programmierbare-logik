LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

Entity count_10 IS
	PORT(clk, clr : IN std_logic;
			number : OUT std_logic_vector(3 downto 0);
			tick : OUT std_logic
	);

END ENTITY;

Architecture mwcnt OF count_10 IS

	Type Zustaende IS (Z0, Z1, Z2, Z3, Z4, Z5, Z6, Z7, Z8, Z9);
	Signal Z, FZ : Zustaende;
Begin

	Zustandsspeicher: PROCESS(clk, clr)
	Begin
		if(clr='1') then
			Z <= Z0;
		elsif (clk'event AND clk='0') then
			Z <= FZ;
		end if;
	End PROCESS Zustandsspeicher;

	Transition : PROCESS(clr, Z)
	Begin
		case Z IS
		when Z0 => FZ <= Z1;
		when Z1 => FZ <= Z2;
		when Z2 => FZ <= Z3;
		when Z3 => FZ <= Z4;
		when Z4 => FZ <= Z5;
		when Z5 => FZ <= Z6;
		when Z6 => FZ <= Z7;
		when Z7 => FZ <= Z8;
		when Z8 => FZ <= Z9;
		when Z9 => FZ <= Z0;
		end case;
	End PROCESS Transition;  
	
	Ausgang : PROCESS(Z)
		Begin
		tick <= '0';
		case Z IS
		when Z0 => number <= "0000";
		when Z1 => number <= "0001";
		when Z2 => number <= "0010";
		when Z3 => number <= "0011";
		when Z4 => number <= "0100";
		when Z5 => number <= "0101";
		when Z6 => number <= "0110";
		when Z7 => number <= "0111";
		when Z8 => number <= "1000";
		when Z9 => 
			number <= "1001";
			tick <= '1';
		end case;
	END PROCESS Ausgang;
	
End mwcnt;