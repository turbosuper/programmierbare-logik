library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;


Entity top_design IS

	PORT(
	clk, en_n, rst, seg_disp_en : IN std_logic;
	seg_m0, seg_m1, seg_s0, seg_s1, seg_h0, seg_h1: out std_logic_vector (0 to 6)
	);
	
END ENTITY;

ARCHITECTURE behaviour OF top_design IS

COMPONENT uhr24 IS
	PORT(
	clk, en_n, rst : IN std_logic;
	DEG_SEC0, DEG_SEC1, DEG_MIN0, DEG_MIN1, DEG_HOUR0, DEG_HOUR1: out std_logic_vector (3 downto 0)
	);
	END COMPONENT;
	
COMPONENT seven_seg_disp IS
	PORT (
	LTN : in std_logic; -- general enable
	BLN : in std_logic; --Test for all segments
	HB : in std_logic;
	D : in std_logic_vector(3 downto 0);
	segm : out std_logic_vector(0 to 6) --Display segments
	);
	END COMPONENT;

SIGNAL high_bit, low_bit, neg_en : std_logic;
SIGNAL D_S0, D_S1, D_M0, D_M1, D_H0, D_H1: std_logic_vector (3 downto 0);
	
BEGIN

	seven_s0: seven_seg_disp
	PORT MAP(
	LTN => seg_disp_en,
	BLN => '0',
	HB => '0',
	D => D_S0,
	segm => seg_s0 --Display segments
	);

	seven_s1: seven_seg_disp
	PORT MAP(
	LTN => seg_disp_en,
	BLN => '0',
	HB => '0',
	D => D_S1,
	segm => seg_s1 --Display segments
	);
	
	seven_m0: seven_seg_disp
	PORT MAP(
	LTN => seg_disp_en,
	BLN => '0',
	HB => '0',
	D => D_M0,
	segm => seg_m0 --Display segments
	);
	
	seven_m1: seven_seg_disp
	PORT MAP(
	LTN => seg_disp_en,
	BLN => '0',
	HB => '0',
	D => D_M1,
	segm => seg_m1 --Display segments
	);

	seven_h0: seven_seg_disp
	PORT MAP(
	LTN => seg_disp_en,
	BLN => '0',
	HB => '0',
	D => D_H0,
	segm => seg_h0 --Display segments
	);

	seven_h1: seven_seg_disp
	PORT MAP(
	LTN => seg_disp_en,
	BLN => '0',
	HB => '0',
	D => D_H1,
	segm => seg_h1 --Display segments
	);

	timer: uhr24
	PORT MAP(clk => clk,
				en_n => en_n,
				rst => rst,
				DEG_SEC0 => D_S0, DEG_SEC1 => D_S1, DEG_MIN0 => D_M0, DEG_MIN1 => D_M1, DEG_HOUR0 => D_H0, DEG_HOUR1 => D_H1
	);
	
	neg_en <= NOT en_n;

END behaviour;