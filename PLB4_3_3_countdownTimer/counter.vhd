library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
 
ENTITY counter IS
    GENERIC(
         last_pulse: positive := 42
     );  
      PORT (
             clk : in std_logic;
             enable_n : in std_logic;
             reset : in std_logic;
             ready : out std_logic := '0' 
         );  
END ENTITY;
 
ARCHITECTURE behaviour OF counter IS
            signal current_pulse: integer range 0 to last_pulse := 0;
BEGIN
             
         process(reset, clk)
            begin
                if (reset = '1') then
                        current_pulse <= 0;
                        ready  <= '0';
                elsif rising_edge(clk) then
                        ready <= '0';
                        if enable_n = '0' then
                           if (current_pulse /= last_pulse-1) then
                                current_pulse <= current_pulse +1; 
                           else
                                ready  <= '1';
                                current_pulse <= 0;
                           end if; 
                        end if;
					end if;	
			end process;
END behaviour;