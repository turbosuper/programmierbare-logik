LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

Entity count_24 IS
	PORT(clk, clr : IN std_logic;
			number : OUT std_logic_vector(7 downto 0);
			tick : OUT std_logic
	);

END ENTITY;

Architecture mwcnt OF count_24 IS

	Type Zustaende IS (Z0, Z1, Z2, Z3, Z4, Z5, Z6, Z7, Z8, Z9, Z10, Z11, Z12, Z13,
							Z14, Z15, Z16, Z17, Z18, Z19, Z20, Z21, Z22, Z23);
	Signal Z, FZ : Zustaende;
Begin

	Zustandsspeicher: PROCESS(clk, clr)
	Begin
		if(clr='1') then
			Z <= Z0;
		elsif (clk'event AND clk='0') then
			Z <= FZ;
		end if;
	End PROCESS Zustandsspeicher;

	Transition : PROCESS(clr, Z)
	Begin
		case Z IS
		when Z0 => FZ <= Z1;
		when Z1 => FZ <= Z2;
		when Z2 => FZ <= Z3;
		when Z3 => FZ <= Z4;
		when Z4 => FZ <= Z5;
		when Z5 => FZ <= Z6;
		when Z6 => FZ <= Z7;
		when Z7 => FZ <= Z8;
		when Z8 => FZ <= Z9;
		when Z9 => FZ <= Z10;
		when Z10 => FZ <= Z11;
		when Z11 => FZ <= Z12;
		when Z12 => FZ <= Z13;
		when Z13 => FZ <= Z14;
		when Z14 => FZ <= Z15;
		when Z15 => FZ <= Z16;
		when Z16 => FZ <= Z17;
		when Z17 => FZ <= Z18;
		when Z18 => FZ <= Z19;
		when Z19 => FZ <= Z20;
		when Z20 => FZ <= Z21;
		when Z21 => FZ <= Z22;
		when Z22 => FZ <= Z23;
		when Z23 => FZ <= Z0;
		end case;
	End PROCESS Transition;  
	
	Ausgang : PROCESS(Z)
		Begin
		tick <= '0';
		case Z IS
		when Z0 => number <= "00000000";
		when Z1 => number <= "00000001";
		when Z2 => number <= "00000010";
		when Z3 => number <= "00000011";
		when Z4 => number <= "00000100";
		when Z5 => number <= "00000101";
		when Z6 => number <= "00000110";
		when Z7 => number <= "00000111";
		when Z8 => number <= "00001000";
		when Z9 => number <= "00001001";
		when Z10 => number <= "00010000";
		when Z11 => number <= "00010001";
		when Z12 => number <= "00010010";
		when Z13 => number <= "00010011";
		when Z14 => number <= "00010100";
		when Z15 => number <= "00010101";
		when Z16 => number <= "00010110";
		when Z17 => number <= "00010111";
		when Z18 => number <= "00011000";
		when Z19 => number <= "00011001";
		when Z20 => number <= "00100000";
		when Z21 => number <= "00100001";
		when Z22 => number <= "00100010";
		when Z23 => number <= "00100011";
				tick <= '1';
		end case;
	END PROCESS Ausgang;
	
End mwcnt;