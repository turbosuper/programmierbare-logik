library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.numeric_std.all;

Entity uhr24 IS

	PORT(
	clk, en_n, rst : IN std_logic;
	DEG_SEC0, DEG_SEC1, DEG_MIN0, DEG_MIN1, DEG_HOUR0, DEG_HOUR1: out std_logic_vector (3 downto 0)
	);

End Entity;


ARCHITECTURE timecounter of uhr24 IS
	constant maxcount: integer := 49999999; -- "20" only for testing!
--	499999; -- Takt anzahl für 1ms

	COMPONENT counter IS
    GENERIC(
         last_pulse: positive --:= 42
     );  
      PORT (
             clk : in std_logic;
             enable_n : in std_logic;
             reset : in std_logic;
             ready : out std_logic := '0' 
         );  
	END COMPONENT;
	
	COMPONENT count_10 IS
	PORT(clk, clr : IN std_logic;
			number : OUT std_logic_vector(3 downto 0);
			tick : OUT std_logic
	);
	END COMPONENT;
	
	COMPONENT count_6 IS
	PORT(clk, clr : IN std_logic;
			number : OUT std_logic_vector(3 downto 0);
			tick : OUT std_logic
	);
	END COMPONENT;
	
	COMPONENT count_2 IS
	PORT(clk, clr : IN std_logic;
			number : OUT std_logic_vector(3 downto 0);
			tick : OUT std_logic
	);
	END COMPONENT;

	COMPONENT count_24 IS
	PORT(clk, clr : IN std_logic;
			number : OUT std_logic_vector(7 downto 0);
			tick : OUT std_logic
	);
	
	END COMPONENT;
	SIGNAL miliTick, sec0Tick, sec1Tick, min0Tick, min1Tick, hoursTick : std_logic;
	SIGNAL DEG_HOURS : std_logic_vector (7 downto 0);
	
	--SIGNAL DEG_SEC0, DEG_SEC1, DEG_MIN0, DEG_MIN1, DEG_HOUR0, DEG_HOUR1 : std_logic_vector(3 downto 0);

	--SIGNAL INT_SEC0, INT_SEC1, INT_MIN0, INT_MIN1, INT_H0, INT_H1 : integer; -- signals only for testing!
	
Begin
	msec_counter : counter
	GENERIC MAP( last_pulse => maxcount)
	PORT MAP(clk => clk,
				enable_n => en_n,
				reset => rst,
				ready => miliTick
	);
	
	sec0 : count_10
	PORT MAP(clk => miliTick,
				clr => rst,
				number => DEG_SEC0,
				tick => sec0Tick	
	);
	
	sec1 : count_6
	PORT MAP(clk => sec0Tick,
				clr => rst,
				number => DEG_SEC1,
				tick => sec1Tick	
	);
		
	min0 : count_10
	PORT MAP(clk => sec1Tick,
				clr => rst,
				number => DEG_MIN0,
				tick => min0Tick	
	);
	
	min1 : count_6
	PORT MAP(clk => min0Tick,
				clr => rst,
				number => DEG_MIN1,
				tick => min1Tick	
	);
	
	hours : count_24
	PORT MAP(clk => min1Tick,
				clr => rst,
				number => DEG_HOURS,
				tick => hoursTick	
	);
	
	DEG_HOUR0 <= DEG_HOURS(3 downto 0);
	DEG_HOUR1 <= DEG_HOURS (7 downto 4);
	
End timecounter; 