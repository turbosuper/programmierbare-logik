library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

ENTITY falling_edge_JKff IS
	PORT
	(j : in std_logic;
	k : in std_logic;
	clear : in std_logic
	clk : in std_logic
	q : out std_logic; 
);
	
ARCHITECTURE behaviour OF falling_edge_JKff IS

SIGNAL q_int: std_logic;

BEGIN

	q_int <= q;

	PROCESS(clk,clear)
	BEGIN
	IF (clear = '1') THEN
		q <= '0';
	ELSIF falling_edge(clk) THEN
		IF (j = '1' AND k = '1') THEN
			q <= not q_int;
		ELSIF (	j = '1' AND k = '0') THEN
			q <= '1';
		ELSIF (	j = '0' AND k = '1') THEN
			q <= '0';
		ELSE 
			q <= q_int;
		END IF;
	ELSE
		q <= q_int;
	END IF;	
	END PROCESS;
	
END behaviour;
