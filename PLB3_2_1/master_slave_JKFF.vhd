library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

ENTITY master_slave_JKFF IS
	PORT (
	clk : in std_logic;
	clk_en : in std_logic;
	kill : in std_logic;
	jump : in std_logic;
	reset : in std_logic;
	q : out std_logic; 
	notq : out std_logic
	);
END ENTITY;

ARCHITECTURE behaviour OF master_slave_JKFF IS

COMPONENT falling_edge_Dff IS
	PORT(
	d : in std_logic;
	clear : in std_logic;
	clk : in std_logic;
	q : out std_logic 
	);

END COMPONENT ;

COMPONENT rising_edge_JKff IS
	PORT(
	j : in std_logic;
	k : in std_logic;
	clk : in std_logic;
	clear : in std_logic;
	q : out std_logic 
	);

END COMPONENT ;

SIGNAL Q1, Q2, enclk : std_logic;
	
BEGIN

	masterFF : rising_edge_JKff
	PORT MAP(
				j => jump,
				k => kill,
				q => Q1,
				clk => enclk,
				clear => reset
	);

	slaveFF : falling_edge_Dff
	PORT MAP(
				d => Q1,
				q => Q2,
				clk => enclk,
				clear => reset
	);
	
	enclk <= clk and clk_en;
	q <= Q2;
	notq <= NOT Q2;
	
END behaviour;
