library ieee;                                                                                                                            
use ieee.std_logic_1164.all;                                                                                                             
                                                                                                                                         
ENTITY testbench_JKflipflops is                                                                                                         
                                                                                                                                         
END testbench_JKflipflops;                                                                                                              
                                                                                                                                         
                                                                                                                                         
ARCHITECTURE test OF testbench_JKflipflops IS                                                                                           
                                                                                                                                         
	COMPONENT rising_edge_JKff IS                                                                                                                
        PORT (                                                                                                                           
                j, k, clear, clk : in std_logic;                                                                                               
                q : out std_logic                                                                                                      
                    );                                                                                                                   
	END COMPONENT;                                                                                                                           
	
	COMPONENT falling_edge_Dff IS                                                                                                                
        PORT (                                                                                                                           
                d, clear, clk : in std_logic;                                                                                               
                q : out std_logic                                                                                                      
                    );                                                                                                                   
                                                                                                                                         
	END COMPONENT;                                                                                                                           
                                                                                                                                         
                                                                                                                                         
	constant period: time := 2 ns;
	SIGNAL tbrising_j : std_logic := '0';                                                                                                           
	SIGNAL tbrising_k : std_logic := '0';                                                                                                           
	SIGNAL tb_clear : std_logic := '0';                                                                                                           
	SIGNAL tb_clk : std_logic := '0';                                                                                                           
	SIGNAL tbrising_q : std_logic := '0';                                                                                                           
	SIGNAL tbfalling_d : std_logic := '0';                                                                                                           
	SIGNAL tbfalling_q : std_logic := '0';                                                                                                           
                                                                                                                                         
	BEGIN                                                                                                                                    
                                                                                                                                         
	dut: rising_edge_JKff                                                                                                                        
	PORT MAP ( 
	j =>	tbrising_j                                                                                                                               
	k => 	tbrising_k 
	clear =>tbrising_clear
	clk => 	tb_clk
	q =>	tbrising_q                                                                                                                                
     	 );                                                                                                                                 

	dut: rising_edge_JKff                                                                                                                        
	PORT MAP ( 
	d => 	tbfalling_d 
	clear =>tb_clear
	clk => 	tb_clk
	q =>	tbfalling_q                                                                                                                                
     	 );                                                                                                                                 
         
tb_clk <= not clk AFTER period;
tb_clear <= '1' AFTER 10 ns, '0' AFTER 14ns, '1' AFTER 56ns, '0' AFTER 67 ns,'1' AFTER 80 ns, '0' AFTER 85ns;
tbrising_j <= '1' AFTER 16ns, '0' AFTER 32ns,'1' AFTER 43ns, '0' AFTER 52ns,'1' AFTER 66ns, '0' AFTER 72ns,'1' AFTER 86ns, '0' AFTER 92ns;
tbrising_k <= '1' AFTER 10ns, '0' AFTER 27ns, '1' AFTER 36ns, '0' AFTER 47ns,'1' AFTER 56ns, '0' AFTER 73ns, '1' AFTER 87ns;
tbfalling_d <= '1' AFTER 30ns, '0' AFTER 40ns, '1' AFTER 55ns, '0' AFTER 65ns,'1' AFTER 85ns, '0' AFTER 93ns, '1' AFTER 97ns;
	 

end test;
