LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

Entity sequnce IS

	PORT(x, clk, clr : IN std_logic;
			y : OUT std_logic;
			LED : OUT std_logic_vector(8 downto 0)
	);

END ENTITY;

Architecture SeqFSM OF sequnce IS

	Type Zustaende IS (A , B , C , D , E , F , G , H , I);
	Signal Z, FZ : Zustaende;
	Signal StatusWord : std_logic_vector(8 downto 0) := "000000000";
Begin

	Zustandsspeicher: PROCESS(clk, clr)
	Begin
		if(clr='0') then
			Z <= A;
		elsif (clk'event AND clk='0') then
			Z <= FZ;
		end if;
	End PROCESS Zustandsspeicher;

	Transition : PROCESS(X, Z)
	Begin
		StatusWord <= "000000000";
		case Z IS
		when A =>
			if(X='0') then 
				FZ <= B;
			else 
				FZ <= F;
			end if;
			StatusWord(0) <= '1';
		when B =>
			if(X='0') then 
				FZ <= C;
			else 
				FZ <= F;
			end if;
			StatusWord(1) <= '1';
		when C =>
			if(X='0') then 
				FZ <= D;
			else 
				FZ <= F;
			end if;
			StatusWord(2) <= '1';
		when D =>
			if(X='0') then 
				FZ <= E;
			else 
				FZ <= F;
			end if;
			StatusWord(3) <= '1';
		when E =>
			if(X='1') then 
				FZ <= F;
			end if;
			StatusWord(4) <= '1';
		when F =>
			if(X='1') then 
				FZ <= G;
			else
				FZ <= B;
			end if;
			StatusWord(5) <= '1';
		when G =>
			if(X='1') then 
				FZ <= H;
			else
				FZ <= B;
			end if;
			StatusWord(6) <= '1';
		when H =>
			if(X='1') then 
				FZ <= I;
			else
				FZ <= B;
			end if;
			StatusWord(7) <= '1';
		when I =>
			if(X='0') then 
				FZ <= B;
			end if;
			StatusWord(8) <= '1';
		when others =>
			FZ <= A;
			StatusWord(0) <= '1';
		end case;
	End PROCESS Transition;
	
	Ausgang : PROCESS(Z)
		Begin
		case Z IS
		when E => 
			y <= '1';
		when I => 
			y <= '1';
		when others =>
			y <= '0';
		end case;
	END PROCESS Ausgang;
	
	LED <= StatusWord;
	
End SeqFSM;