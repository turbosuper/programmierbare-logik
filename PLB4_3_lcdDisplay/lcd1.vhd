library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity lcd1 is
port (
		clk, rst, ld : in std_logic;
		data : in std_logic_vector(3 downto 0);
		bcd7 : out std_logic_vector(0 to 6); 
		gray, aiken: out std_logic_vector(3 downto 0);
		rs, e, r_nw : out std_logic;
		db : out std_logic_vector( 7 downto 0));
end lcd1;

architecture behav of lcd1 is
--enable
--50 MHz 2000 tacts come after 40us
signal count: integer range 0 to 49999999;
constant maxcount: integer := 49999998; --1000ms
--constant maxcount: integer := 4998; -- 100us
signal enable: boolean;
--BCD-Counter
signal bcd_count, lgray, laiken: std_logic_vector(3 downto 0);

--Automat
--type Zustaende is (fs1,fs2,fs3, emode, donoff, dclear, en1, en2, en3, en4, en5, en6); 
type Zustaende is (Z0, Z1, Z2, Z3, Z4, Z5, Z6, Z7,Z8, Z9,Z10, 
				Z11, Z12, Z13, Z14, Z15, Z16,Z17,Z18,Z19,Z20,Z21,Z22,Z23,Z24,
				Z25,Z26,Z27,Z28,Z29,Z30,Z31,Z32,Z33,Z34,Z35,Z36,Z37,Z38,Z39,
				Z40,Z41,Z42,Z43,Z44,Z45,Z46,Z47,Z48,Z49,Z50,Z51,Z52,Z53,Z54,
				Z55,Z56,Z57,Z58,Z59,Z60,Z61,Z62,Z63,Z64,Z65,Z66,Z67,Z68,Z69,
				Z70,Z71,Z72,Z73,Z74,Z75,Z76,Z77,Z78,Z79,Z80,Z81,Z82,Z83,Z84,
				Z85,Z86,Z87,Z88,Z89,Z90,Z91,Z92,Z93,Z94,Z95,Z96,Z97,Z98,Z99,
				Z100,Z101,Z102,Z103,Z58a); -- Aufzählung
signal z_neu, z: Zustaende;
signal delay: integer range 0 to 1999999;
--signal aikenONlcd: std_logic_vector (3 downto 0);

begin
-- enable
process(clk)
begin
	if rising_edge(clk) then
		if rst = '1' then
			count <= 0;
			enable <= false;
		elsif count > maxcount then
			count <= 0;
			enable <= true;
		else
			count <= count + 1;
			enable <= false;
		end if;
	end if;
end process;

--BCD-Counter
process(clk)
begin
	if rising_edge(clk) then
		if rst = '1' then
			bcd_count <= (others => '0');
		elsif enable then
			if ld = '1' then
				if data > "1001" then
					bcd_count <= (others => '0');
				else
					bcd_count <= data;
				end if;
			elsif bcd_count = "1001" then
				bcd_count <= (others => '0');
			else
				bcd_count <= bcd_count + 1;
			end if;
		end if;
	end if;
end process;

--Gray Aiken
conv: process(bcd_count)
begin
	case bcd_count is
		when "0000" => lgray <= "0000"; laiken <= "0000"; 
		when "0001" => lgray <= "0001"; laiken <= "0001";
		when "0010" => lgray <= "0011"; laiken <= "0010";
		when "0011" => lgray <= "0010"; laiken <= "0011";
		when "0100" => lgray <= "0110"; laiken <= "0100";
		when "0101" => lgray <= "0111"; laiken <= "1011";
		when "0110" => lgray <= "0101"; laiken <= "1100";
		when "0111" => lgray <= "0100"; laiken <= "1101";
		when "1000" => lgray <= "1100"; laiken <= "1110";
		when others => lgray <= "1101"; laiken <= "1111";
	end case;
end process;
--Ausgabe 
--bcd_7segm: entity work.siebensegm(behav) port map(bcd_count, bcd7);
gray <= lgray;
aiken <= laiken;

--Automat

--Zustandsübergang 
z_uebergang: process(clk, rst)
begin
	if rst = '1' then
		z <= Z0;
		delay <= 799999;
	elsif rising_edge(clk) then
	  --if enable then
	   if delay = 0 then
			z <= z_neu;
			case z_neu is
			 when Z0 => delay <= 799999; -- 16ms
			 when Z1|Z4|Z7|Z10|Z13|Z16|Z19|Z22|Z25|Z28|Z31|Z34|Z37|Z40|Z43|Z46|Z49|Z52|Z55|Z58|Z60|Z63|Z66|Z69|Z72|Z75|Z78|Z81|Z84|Z87|Z90|Z93|Z96|Z99|Z102 => delay <= 11; --240ns
			 when Z3|Z6|Z9|Z15|Z18|Z21|Z24|Z27|Z30|Z33|Z36|Z39|Z42|Z45|Z48|Z51|Z54|Z57|Z59|Z62|Z65|Z68|Z71|Z74|Z77|Z80|Z83|Z86|Z89|Z92|Z95|Z98|Z101 => delay <= 1999; --40us
			 when Z12 => delay <= 79999; --1,6 ms
			 when Others => delay <= 0;
			end case;
		else
			delay <= delay - 1;
		end if;
	  --end if;
	end if;
end process;
-- z_neu bestimmen in Huffmann Normalform(mit Ausgang)
zneu: process(z, bcd_count)
begin
	rs <= '0';db <= "00111000";e <= '0';
	case z is
		when Z0 => z_neu <= Z1;--40ms und Defaultwerte
		when Z1 => z_neu <= Z2;e <= '1'; -- 240ns
		when Z2 => z_neu <= Z3; -- 20ns Holdtime
		when Z3 => z_neu <= Z4; -- 40us Befahlsabarbeitung
		when Z4 => z_neu <= Z5;e <= '1'; --240ns
		when Z5 => z_neu <= Z6; -- 20ns Holdtime
		when Z6 => z_neu <= Z7;db <= "00001111"; -- Display on 40us
		when Z7 => z_neu <= Z8;e <= '1'; db <= "00001111";-- 240ns
		when Z8 => z_neu <= Z9; db <= "00001111";-- 20ns Holdtime
		when Z9 => z_neu <= Z10;db <= "00000001"; -- Display clear 40us(vom functionset)
		when Z10 => z_neu <= Z11;e <= '1';db <= "00000001"; -- 240ns
		when Z11 => z_neu <= Z12;db <= "00000001";-- 20ns Holdtime
		when Z12 => z_neu <= Z13;db <= "00000110"; -- Entrymode 1,6ms(Display Clear)
		when Z13 => z_neu <= Z14;e <= '1'; db <= "00000110"; --240ns
		when Z14 => z_neu <= Z15;db <= "00000110"; --20ns Holdtime
		
		--..
		when Z15 => z_neu <= Z16;rs <= '1';db <= "01000001" ; --'A' schreiben 40us warten
		when Z16 => z_neu <= Z17;e <= '1';rs <= '1';db <= "01000001"; --240ns
		when Z17 => z_neu <= Z18;rs <= '1';db <= "01000001" ;--20ns
		-- ...
		when Z18 => z_neu <= Z19;rs <= '1';db <= "01001001" ; --'I' schreiben 40us warten
		when Z19 => z_neu <= Z20;e <= '1';rs <= '1';db <= "01001001"; --240ns
		when Z20 => z_neu <= Z21;rs <= '1';db <= "01001001" ;--20ns
		--..
		when Z21 => z_neu <= Z22;rs <= '1';db <= "01001011" ; --'K' schreiben 40us warten
		when Z22 => z_neu <= Z23;e <= '1';rs <= '1';db <= "01001011"; --240ns
		when Z23 => z_neu <= Z24;rs <= '1';db <= "01001011" ;--20ns
		--..
		when Z24 => z_neu <= Z25;rs <= '1';db <= "01000101" ; --'E' schreiben 40us warten
		when Z25 => z_neu <= Z26;e <= '1';rs <= '1';db <= "01000101"; --240ns
		when Z26 => z_neu <= Z27;rs <= '1';db <= "01000101" ;--20ns

		--..
		when Z27 => z_neu <= Z28;rs <= '1';db <= "01001110" ; --'N' schreiben 40us warten
		when Z28 => z_neu <= Z29;e <= '1';rs <= '1';db <= "01001110"; --240ns
		when Z29 => z_neu <= Z30;rs <= '1';db <= "01001110" ;--20ns
		
		--..
		when Z30 => z_neu <= Z31;rs <= '1';db <= "00100000" ; --Leerzechen zeigen 40us warten
		when Z31 => z_neu <= Z32;e <= '1';rs <= '1';db <= "00100000"; --240ns
		when Z32 => z_neu <= Z33;rs <= '1';db <= "00100000" ;--20ns
	
		--..
		when Z33 => z_neu <= Z34;rs <= '1';db <= "01000111" ; --'G' schreiben 40us warten
		when Z34 => z_neu <= Z35;e <= '1';rs <= '1';db <= "01000111"; --240ns
		when Z35 => z_neu <= Z36;rs <= '1';db <= "01000111" ;--20ns
	
		--..
		when Z36 => z_neu <= Z37;rs <= '1';db <= "01010010" ; --'R' schreiben 40us warten
		when Z37 => z_neu <= Z38;e <= '1';rs <= '1';db <= "01010010"; --240ns
		when Z38 => z_neu <= Z39;rs <= '1';db <= "01010010" ;--20ns
	
		--..
		when Z39 => z_neu <= Z40;rs <= '1';db <= "01000001" ; --'A' schreiben 40us warten
		when Z40 => z_neu <= Z41;e <= '1';rs <= '1';db <= "01000001"; --240ns
		when Z41 => z_neu <= Z42;rs <= '1';db <= "01000001" ;--20ns
	
		--..
		when Z42 => z_neu <= Z43;rs <= '1';db <= "01011001" ; --'Y' schreiben 40us warten
		when Z43 => z_neu <= Z44;e <= '1';rs <= '1';db <= "01011001"; --240ns
		when Z44 => z_neu <= Z45;rs <= '1';db <= "01011001" ;--20ns
	
		--..
		when Z45 => z_neu <= Z46;rs <= '1';db <= "00100000" ; --Leerzeichen schreiben 40us warten
		when Z46 => z_neu <= Z47;e <= '1';rs <= '1';db <= "00100000"; --240ns
		when Z47 => z_neu <= Z48;rs <= '1';db <= "00100000" ;--20ns
	
		--..
		when Z48 => z_neu <= Z49;rs <= '1';db <= "01000010" ; --'B' schreiben 40us warten
		when Z49 => z_neu <= Z50;e <= '1';rs <= '1';db <= "01000010"; --240ns
		when Z50 => z_neu <= Z51;rs <= '1';db <= "01000010" ;--20ns
	
		--..
		when Z51 => z_neu <= Z52;rs <= '1';db <= "01000011" ; --'C' schreiben 40us warten
		when Z52 => z_neu <= Z53;e <= '1';rs <= '1';db <= "01000011"; --240ns
		when Z53 => z_neu <= Z54;rs <= '1';db <= "01000011" ;--20ns
	
		--..
		when Z54 => z_neu <= Z55;rs <= '1';db <= "01000100" ; --'D' schreiben 40us warten
		when Z55 => z_neu <= Z56;e <= '1';rs <= '1';db <= "01000100"; --240ns
		when Z56 => z_neu <= Z57;rs <= '1';db <= "01000100" ;--20ns
	
		--second row intialise
		when Z57 => z_neu <= Z58;rs <= '0'; db <= "11000000" ;--40 us
		when Z58 => z_neu <= Z58a; e <= '1'; db <= "11000000";-- 240ns
		when Z58a => z_neu <= Z59;rs <= '1';db <= "11000000" ;--20ns
		
		--second row AIKEN write
		when Z59 => z_neu <= Z60;rs <= '1';db <= "0011000" & laiken(3) ; --'D' schreiben 40us warten
		when Z60 => z_neu <= Z61;e <= '1';rs <= '1';db <= "0011000" & laiken(3); --240ns
		when Z61 => z_neu <= Z62;rs <= '1';db <= "0011000" & laiken(3) ;--20ns
		
		when Z62 => z_neu <= Z63;rs <= '1';db <= "0011000" & laiken(2) ; --'D' schreiben 40us warten
		when Z63 => z_neu <= Z64;e <= '1';rs <= '1';db <= "0011000" & laiken(2); --240ns
		when Z64 => z_neu <= Z65;rs <= '1';db <= "0011000" & laiken(2) ;--20ns
				
		when Z65 => z_neu <= Z66;rs <= '1';db <= "0011000" & laiken(1) ; --'D' schreiben 40us warten
		when Z66 => z_neu <= Z67;e <= '1';rs <= '1';db <= "0011000" & laiken(1); --240ns
		when Z67 => z_neu <= Z68;rs <= '1';db <= "0011000" & laiken(1) ;--20ns
	
		when Z68 => z_neu <= Z69;rs <= '1';db <= "0011000" & laiken(0) ; --'D' schreiben 40us warten
		when Z69 => z_neu <= Z70;e <= '1';rs <= '1';db <= "0011000" & laiken(0); --240ns
		when Z70 => z_neu <= Z71;rs <= '1';db <= "0011000" & laiken(0) ;--20ns
		
		--second row spaces write
		
		when Z71 => z_neu <= Z72;rs <= '1';db <= "00100000" ; --Leerzeichen schreiben 40us warten
		when Z72 => z_neu <= Z73;e <= '1';rs <= '1';db <= "00100000"; --240ns
		when Z73 => z_neu <= Z74;rs <= '1';db <= "00100000" ;--20ns
	
		when Z74 => z_neu <= Z75;rs <= '1';db <= "00100000" ; --Leerzeichen schreiben 40us warten
		when Z75 => z_neu <= Z76;e <= '1';rs <= '1';db <= "00100000"; --240ns
		when Z76 => z_neu <= Z77;rs <= '1';db <= "00100000" ;--20ns
		
		--second row Gray write
		when Z77 => z_neu <= Z78;rs <= '1';db <= "0011000" & lgray(3) ; --'D' schreiben 40us warten
		when Z78 => z_neu <= Z79;e <= '1';rs <= '1';db <= "0011000" & lgray(3); --240ns
		when Z79 => z_neu <= Z80;rs <= '1';db <= "0011000" & lgray(3) ;--20ns
		
		when Z80 => z_neu <= Z81;rs <= '1';db <= "0011000" & lgray(2) ; --'D' schreiben 40us warten
		when Z81 => z_neu <= Z82;e <= '1';rs <= '1';db <= "0011000" & lgray(2); --240ns
		when Z82 => z_neu <= Z83;rs <= '1';db <= "0011000" & lgray(2) ;--20ns
				
		when Z83 => z_neu <= Z84;rs <= '1';db <= "0011000" & lgray(1) ; --'D' schreiben 40us warten
		when Z84 => z_neu <= Z85;e <= '1';rs <= '1';db <= "0011000" & lgray(1); --240ns
		when Z85 => z_neu <= Z86;rs <= '1';db <= "0011000" & lgray(1) ;--20ns
	
		when Z86 => z_neu <= Z87;rs <= '1';db <= "0011000" & lgray(0) ; --'D' schreiben 40us warten
		when Z87 => z_neu <= Z88;e <= '1';rs <= '1';db <= "0011000" & lgray(0); --240ns
		when Z88 => z_neu <= Z89;rs <= '1';db <= "0011000" & lgray(0) ;--20ns
		
		--second row space write
		
		when Z89 => z_neu <= Z90;rs <= '1';db <= "00100000" ; --Leerzeichen schreiben 40us warten
		when Z90 => z_neu <= Z91;e <= '1';rs <= '1';db <= "00100000"; --240ns
		when Z91 => z_neu <= Z92;rs <= '1';db <= "00100000" ;--20ns
		
		--second row BCD write
		when Z92 => z_neu <= Z93;rs <= '1';db <= "0011000" & bcd_count(3) ; --'D' schreiben 40us warten
		when Z93 => z_neu <= Z94;e <= '1';rs <= '1';db <= "0011000" & bcd_count(3); --240ns
		when Z94 => z_neu <= Z95;rs <= '1';db <= "0011000" & bcd_count(3) ;--20ns
		
		when Z95 => z_neu <= Z96;rs <= '1';db <= "0011000" & bcd_count(2) ; --'D' schreiben 40us warten
		when Z96 => z_neu <= Z97;e <= '1';rs <= '1';db <= "0011000" & bcd_count(2); --240ns
		when Z97 => z_neu <= Z98;rs <= '1';db <= "0011000" & bcd_count(2) ;--20ns
				
		when Z98 => z_neu <= Z99;rs <= '1';db <= "0011000" & bcd_count(1) ; --'D' schreiben 40us warten
		when Z99 => z_neu <= Z100;e <= '1';rs <= '1';db <= "0011000" & bcd_count(1); --240ns
		when Z100 => z_neu <= Z101;rs <= '1';db <= "0011000" & bcd_count(1) ;--20ns
	
		when Z101 => z_neu <= Z102;rs <= '1';db <= "0011000" & bcd_count(0) ; --'D' schreiben 40us warten
		when Z102 => z_neu <= Z103;e <= '1';rs <= '1';db <= "0011000" & bcd_count(0); --240ns
		when Z103 => z_neu <= Z57;rs <= '1';db <= "0011000" & bcd_count(0) ;--20ns
		
	
	end case;
end process;
r_nw <= '0'; -- es wird nur geschrieben!!
end architecture;
		