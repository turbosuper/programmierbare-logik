library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

Entity booth IS
	Port(
			opA, opB : in std_logic_vector(3 downto 0);
			rst, clk : in std_logic;
			result : out std_logic_vector(6 downto 0);
			okled : out std_logic;
			zled : out std_logic_vector(5 downto 0);
			cinc : out std_logic_vector(1 downto 0)
	);

End Entity;

ARCHITECTURE calculate  OF booth IS

	type zustaende is (zero, multiplicator_build, compare,  add, subst,  count_comp, shift_r, create_result, count_inc);
	SIGNAL Z, FZ : zustaende;
	SIGNAL opB2erComp : std_logic_vector(3 downto 0);
	SIGNAL counter : integer := 0;
	SIGNAL opB_temp: std_logic_vector (8 downto 0);
	SIGNAL temp_result: std_logic_vector (8 downto 0);
	SIGNAL dummy : std_logic;
	
BEGIN 

--	opB2erComp <= not opB + 1;
	
	z_speicher: process(clk, rst)
	BEGIN
		if rst = '1' THEN
			Z <= zero;
		ELSIF rising_edge(clk) THEN
			z <= FZ;
		END IF;
	END PROCESS;
			
	Transition : PROCESS(Z, counter)
	BEGIN
		CASE Z IS
		WHEN zero => 
				FZ <= multiplicator_build;
		
		WHEN multiplicator_build =>
			FZ <= compare;
			
		WHEN compare =>
			IF (temp_result(0) = '0') AND (temp_result(1) = '1') THEN
					FZ <= subst;
			ELSIF (temp_result(0) = '1') AND (temp_result(1) = '0') THEN
					FZ <= add;
			ELSE
					FZ <= count_comp; 
			END IF;
		
		WHEN add =>
				FZ <= count_comp;
		
		WHEN subst =>
				FZ <= count_comp;	
		
		WHEN count_comp =>
			IF counter = 3 THEN 
				FZ <= create_result;
			ELSE
				FZ <= shift_r;
			END IF;
			
		WHEN create_result =>
			FZ <= zero;
		
		WHEN count_inc =>
			FZ <= compare;
			
		WHEN shift_r =>
		 FZ <= count_inc;
 		
		END CASE;
	END PROCESS;
		
	ausggang: PROCESS(Z, clk)
	BEGIN
	if rising_edge(clk) THEN
		case Z is
		WHEN zero => 
			result <= (others => '0');
			counter <= 0;
			okled <= '0';
			zled <= "000000";
		WHEN multiplicator_build =>
				temp_result <= "0000" & opA & '0';
				zled <= "000001";
		WHEN add =>
			temp_result(8 downto 5) <= temp_result(8 downto 5) + opB;
			zled <= "000011";
		WHEN subst =>
			temp_result(8 downto 5) <= temp_result(8 downto 5) - opB;
			zled <= "000100";
		WHEN count_inc =>
			IF counter = 0 THEN
				cinc <= "00";
			elsif counter = 1 THEN
				cinc <= "01";
			elsif counter = 2 THEN
				cinc <= "10";
			elsif counter = 3 THEN
				cinc <= "11";
			end if;
			temp_result <= opB_temp;
			counter <=  counter + 1;
			zled <= "001001";
		WHEN shift_r =>
			opB_temp <= temp_result(8) & temp_result(8 downto 1);
			zled <= "000110";
		WHEN create_result =>
			result <= temp_result (8 downto 2);
			okled <= '1';
			zled <= "000111";
		WHEN compare =>
			zled <= "000010";
		WHEN count_comp =>
			zled <= "000101";
		WHEN OTHERS =>
			dummy <= '0';
		END CASE;
	END IF;
	
	END PROCESS;

END calculate;
