library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
 
ENTITY Comb_logic_3 IS
    PORT (
    x : in std_logic;
    y : in std_logic;
    z : in std_logic;
    f : out std_logic
    );
END Comb_logic_3;

ARCHITECTURE behave OF Comb_logic_3 IS

BEGIN

	f <= (y and x) or (not y and z and x) or (y and z and not x);

END behave;