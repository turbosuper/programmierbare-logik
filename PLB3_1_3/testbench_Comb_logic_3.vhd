library ieee;                                                                                                                            
use ieee.std_logic_1164.all;                                                                                                             
                                                                                                                                         
ENTITY testbench_Comb_logic_3 is                                                                                                         
                                                                                                                                         
END testbench_Comb_logic_3;                                                                                                              
                                                                                                                                         
                                                                                                                                         
ARCHITECTURE test OF testbench_Comb_logic_3 IS                                                                                           
                                                                                                                                         
	COMPONENT Comb_logic_3 IS                                                                                                                
        PORT (                                                                                                                           
                x, y, z : in std_logic;                                                                                               
                f : out std_logic                                                                                                      
                    );                                                                                                                   
                                                                                                                                         
	END COMPONENT;                                                                                                                           
                                                                                                                                         
                                                                                                                                         
	constant small_period: time := 20 ns;
	constant middle_period: time :=80 ns;
	constant big_period: time := 160 ns;
	constant longest_period: time := 640 ns;
	SIGNAL tbx : std_logic := '0';                                                                                                           
	SIGNAL tby : std_logic := '0';                                                                                                           
	SIGNAL tbz : std_logic := '0';                                                                                                           
	SIGNAL tbv : std_logic := '0';                                                                                                           
	SIGNAL tbf : std_logic := '0';                                                                                                           
                                                                                                                                         
	BEGIN                                                                                                                                    
                                                                                                                                         
	dut: Comb_logic_3                                                                                                                        
                                                                                                                                         
                                                                                                                                         
	PORT MAP (                                                                                                                               
		x => tbx,                                                                                                                              
		y => tby,                                                                                                                              
		z => tbz,                                                                                                                            
		f => tbf                                                                                                                               
      );                                                                                                                                 
                                                                                                                                         
tbx <= not tbx AFTER longest_period;
tby <=  not tby AFTER big_period;
tbv <=  not tbv AFTER middle_period;
tbz <=  not tbz AFTER small_period;

end test;
